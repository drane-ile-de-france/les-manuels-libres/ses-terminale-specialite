<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
 <head>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="canonical" href="https://www.sciencespo.fr/department-economics/econofides"> 
  <meta property="og:title" content="Chapitre 2 Quels sont les fondements du commerce international et de l’internationalisation de la production&nbsp;?"> 
  <meta property="og:type" content="website"> 
  <meta property="og:url" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/text/02.html"> 
  <meta property="og:description" content="Comment les échanges internationaux en sont-t-ils venus à être dominés par des entreprises multinationales et comment le commerce international induit-t-il des effets importants sur le partage des richesses dans le monde&nbsp;?"> 
  <meta property="og:image" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/images/web/chapter-02-header.jpg"> 
  <link rel="stylesheet" type="text/css" media="all" href="https://cdn-static.pearltrees.com/econofides/terminale-ses/styles/web.css@date=20210916114607.css"> 
  <script>
        function delete_cookie(name) {
            document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
            console.log('Expired and removed MathJax menu cookie.');
        }
        delete_cookie('mjx.menu')
    </script> 
  <script id="MathJaxConfig" type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    "HTML-CSS": {
                        availableFonts: [], // force use of webFont
                        webFont: "Gyre-Pagella",
                        scale: 90,
                        styles: {
                            ".MathJax [style*=border-top-width]": {
                                "border-top-width": "0.5pt ! important"
                            }
                        }
                    }
                });
            </script> 
  <script id="MathJax" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML">
            </script>  
  <!-- End Matomo Code --> 
  <style> #MathJax_Message { display: none} .medals{display:none;}
</style>
  <title>Pearltrees :Question 2.20 Complétez le texte</title>
  <title></title>
 </head> 
 <body class="terminale-ses chapter" data-title="L’Économie pour Terminale SES" style="background-color:white;">
  <div id="wrapper">
   <div id="content">
    <div class="question" data-question="question-02-20">  
     <p>Supposez qu’il y ait seulement deux pays dans le monde, l’Allemagne et la Turquie, chacun ayant le même nombre de travailleurs. Au cours d’une période donnée, chaque travailleur en Allemagne peut produire trois automobiles ou vingt télévisions, chaque travailleur en Turquie peut produire deux automobiles ou trente télévisions.</p> 
     <ol> 
      <li>En Allemagne, le prix relatif des automobiles en l’absence de commerce international est de <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="vutfky8w">0,15</option> <option data-select-code="ikjcxdtq">3</option> <option data-select-code="83c73mxm">6,67</option> </select></span> <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="mebyb3cd">automobiles par télévision</option> <option data-select-code="r3zo5ryh">télévisions par automobile</option> <option data-select-code="q1fahnqa">euros</option> </select></span> alors qu’en Turquie il est de <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="wh9trmet">0,07</option> <option data-select-code="r0lpcq1f">2</option> <option data-select-code="yf191d9j">15</option> </select></span> <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="9rq7q4w1">automobiles par télévision</option> <option data-select-code="7yj796iz">télévisions par automobile</option> <option data-select-code="hjuacq5v">euros</option> </select></span>.</li> 
      <li>Supposez maintenant que l’Allemagne et la Turquie s’ouvrent à l’échange&nbsp;: le prix relatif mondial des automobiles sera alors compris (en l’absence de coûts à l’échange) entre <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="6tlqhpnm">3</option> <option data-select-code="2zwe6nsy">6,67</option> <option data-select-code="7nsdnrx9">15</option> </select></span> et <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="lw263rvd">15</option> <option data-select-code="97hkhhak">20</option> <option data-select-code="a3ppdaj3">30</option> </select></span>.</li> 
      <li>Si le prix relatif mondial des automobiles est de 10, dans quel bien l’Allemagne va-t-elle se spécialiser&nbsp;? La production <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="mz6u8i69">d’automobiles</option> <option data-select-code="4d66ukl5">de télévisions</option> <option data-select-code="kj1fi65n">des deux produits</option> </select></span>.</li> 
     </ol> 
    </div>
   </div>
   <script src="https://cdn-static.pearltrees.com/econofides/js/bundle.js@date=pearltrees"></script>
   <style>
        .js-mcq .mcq-correct .mcq-feedback:before {
            content: "Correct !";
        }
        .js-mcq .mcq-incorrect .mcq-feedback:before {
            content: "Incorrect";
        }
        .js-mcq .mcq-partially-correct .mcq-feedback:before {
            content: "Vous n'avez pas sélectionné toutes les bonnes réponses.";
        }
    .medals{display:none;}
</style>
  </div>
 </body>
</html>