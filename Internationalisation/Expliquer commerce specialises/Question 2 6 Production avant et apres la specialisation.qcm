<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
 <head>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="canonical" href="https://www.sciencespo.fr/department-economics/econofides"> 
  <meta property="og:title" content="Chapitre 2 Quels sont les fondements du commerce international et de l’internationalisation de la production&nbsp;?"> 
  <meta property="og:type" content="website"> 
  <meta property="og:url" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/text/02.html"> 
  <meta property="og:description" content="Comment les échanges internationaux en sont-t-ils venus à être dominés par des entreprises multinationales et comment le commerce international induit-t-il des effets importants sur le partage des richesses dans le monde&nbsp;?"> 
  <meta property="og:image" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/images/web/chapter-02-header.jpg"> 
  <link rel="stylesheet" type="text/css" media="all" href="https://cdn-static.pearltrees.com/econofides/terminale-ses/styles/web.css@date=20210916114607.css"> 
  <script>
        function delete_cookie(name) {
            document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
            console.log('Expired and removed MathJax menu cookie.');
        }
        delete_cookie('mjx.menu')
    </script> 
  <script id="MathJaxConfig" type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    "HTML-CSS": {
                        availableFonts: [], // force use of webFont
                        webFont: "Gyre-Pagella",
                        scale: 90,
                        styles: {
                            ".MathJax [style*=border-top-width]": {
                                "border-top-width": "0.5pt ! important"
                            }
                        }
                    }
                });
            </script> 
  <script id="MathJax" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML">
            </script>  
  <!-- End Matomo Code --> 
  <style> #MathJax_Message { display: none} .medals{display:none;}
</style>
  <title>Pearltrees :Question 2.6 Complétez le tableau</title>
  <title></title>
 </head> 
 <body class="terminale-ses chapter" data-title="L’Économie pour Terminale SES" style="background-color:white;">
  <div id="wrapper">
   <div id="content">
    <div class="question" data-question="question-02-06">  
     <p>Avant de compléter le tableau, veillez à lire les conseils suivants&nbsp;:</p> 
     <ul> 
      <li>Reportez-vous aux données du <a href="02.html#tableau-2-2">Tableau 2.2</a> pour évaluer la production en autarcie et après la spécialisation.</li> 
      <li>Pour calculer la production une fois la spécialisation réalisée, vous reprendrez le temps total que consacrait Greta à la production agricole, soit 30 heures, et le temps total que consacrait Carlos à la production agricole, soit 90 heures.</li> 
     </ul> 
     <table> 
      <thead> 
       <tr> 
        <th>&nbsp;</th> 
        <th>Greta sur l’île des Délices</th> 
        <th>Carlos sur l’île du Bonheur</th> 
        <th>Monde&nbsp;: l’île des Délices et l’île du Bonheur</th> 
       </tr> 
      </thead> 
      <tbody> 
       <tr> 
        <td>En autarcie&nbsp;: avant la spécialisation et l’échange</td> 
        <td>&nbsp;</td> 
        <td>&nbsp;</td> 
        <td>&nbsp;</td> 
       </tr> 
       <tr> 
        <td>Production de pommes</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="g7x32ckw">1</option> <option data-select-code="ngfjk0yq">2</option> <option data-select-code="2m99xnis">3</option> </select></span> tonne(s)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="qp4y5ukm">1</option> <option data-select-code="clg2p5zi">2</option> <option data-select-code="suruslhw">3</option> </select></span> tonne(s)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ks6hahaz">1</option> <option data-select-code="d1m98agf">2</option> <option data-select-code="bf5pkiuj">3</option> </select></span> tonne(s)</td> 
       </tr> 
       <tr> 
        <td>Production de blé</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="1sil309s">1</option> <option data-select-code="xxg0qw6d">2</option> <option data-select-code="9ippl3q4">3</option> </select></span> tonne(s)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="g9bj0i52">1</option> <option data-select-code="jhgokyun">2</option> <option data-select-code="74umloof">3</option> </select></span> tonne(s)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="bkupps5s">1</option> <option data-select-code="pu98319f">2</option> <option data-select-code="usjgthmw">3</option> </select></span> tonne(s)</td> 
       </tr> 
       <tr> 
        <td>Après la spécialisation et l’échange</td> 
        <td>&nbsp;</td> 
        <td>&nbsp;</td> 
        <td>&nbsp;</td> 
       </tr> 
       <tr> 
        <td>Production de pommes</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="3hznf07x">2</option> <option data-select-code="dfps8kwp">3</option> <option data-select-code="50rzju07">6</option> </select></span> tonne(s)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="0ij90hq0">0</option> <option data-select-code="mrdhzqzi">1</option> <option data-select-code="s6s4hit8">2</option> </select></span> tonne(s)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="a8ceqks9">2</option> <option data-select-code="q07d0jt7">3</option> <option data-select-code="v8wguwln">8</option> </select></span> tonne(s)</td> 
       </tr> 
       <tr> 
        <td>Production de blé</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="131g1vd0">0</option> <option data-select-code="t7z2q2yz">1</option> <option data-select-code="af4hhs3p">2</option> </select></span> tonne(s)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="49m2htb2">2</option> <option data-select-code="ec9f3eip">3</option> <option data-select-code="m2rigmha">6</option> </select></span> tonne(s)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ztteptza">2</option> <option data-select-code="an4p4rop">3</option> <option data-select-code="qud4kvo7">8</option> </select></span> tonne(s)</td> 
       </tr> 
       <tr> 
        <td>Production supplémentaire (surplus)</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="jpe8jats">1</option> <option data-select-code="slqx2brn">2</option> <option data-select-code="a0bfl14d">5</option> </select></span> tonne(s) de pommes</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ljbytghm">1</option> <option data-select-code="nado1waf">2</option> <option data-select-code="9aq6l4b9">5</option> </select></span> tonne(s) de blé</td> 
        <td><span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="s9o344dc">1</option> <option data-select-code="6bcokhhl">2</option> <option data-select-code="u91mfo8k">10</option> </select></span> tonne(s) de pommes <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ivle555g">1</option> <option data-select-code="7jqme2yt">2</option> <option data-select-code="wwuqmb80">10</option> </select></span> tonne(s) de blé</td> 
       </tr> 
      </tbody> 
     </table> 
    </div>
   </div>
   <script src="https://cdn-static.pearltrees.com/econofides/js/bundle.js@date=pearltrees"></script>
   <style>
        .js-mcq .mcq-correct .mcq-feedback:before {
            content: "Correct !";
        }
        .js-mcq .mcq-incorrect .mcq-feedback:before {
            content: "Incorrect";
        }
        .js-mcq .mcq-partially-correct .mcq-feedback:before {
            content: "Vous n'avez pas sélectionné toutes les bonnes réponses.";
        }
    .medals{display:none;}
</style>
  </div>
 </body>
</html>