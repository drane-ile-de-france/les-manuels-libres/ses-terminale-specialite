<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
 <head>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="canonical" href="https://www.sciencespo.fr/department-economics/econofides"> 
  <meta property="og:title" content="Chapitre 5 Quelles politiques économiques dans le cadre européen&nbsp;?"> 
  <meta property="og:type" content="website"> 
  <meta property="og:url" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/text/05.html"> 
  <meta property="og:description" content="Quelles ont été les étapes de la construction du projet européen et comment son organisation d’aujourd’hui peut-t-il engendrer des difficultés quant à la coordination entre les Etats membres pour faire face à des chocs asymétriques&nbsp;?"> 
  <meta property="og:image" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/images/web/chapter-05-header.jpg"> 
  <link rel="stylesheet" type="text/css" media="all" href="https://cdn-static.pearltrees.com/econofides/terminale-ses/styles/web.css@date=20210916114607.css"> 
  <script>
        function delete_cookie(name) {
            document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
            console.log('Expired and removed MathJax menu cookie.');
        }
        delete_cookie('mjx.menu')
    </script> 
  <script id="MathJaxConfig" type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    "HTML-CSS": {
                        availableFonts: [], // force use of webFont
                        webFont: "Gyre-Pagella",
                        scale: 90,
                        styles: {
                            ".MathJax [style*=border-top-width]": {
                                "border-top-width": "0.5pt ! important"
                            }
                        }
                    }
                });
            </script> 
  <script id="MathJax" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML">
            </script>  
  <!-- End Matomo Code --> 
  <style> #MathJax_Message { display: none} .medals{display:none;}
</style>
  <title>Pearltrees :Question 5.7 Vérifiez vos acquis : choisissez le bon terme pour compléter ce texte</title>
  <title></title>
 </head> 
 <body class="terminale-ses chapter" data-title="L’Économie pour Terminale SES" style="background-color:white;">
  <div id="wrapper">
   <div id="content">
    <div class="question" data-question="question-05-07">  
     <p>Si un État membre de la zone euro mène une politique budgétaire trop <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="6zhq8e02">expansive</option> <option data-select-code="v93uy63m">rigoureuse</option> <option data-select-code="bw01llbb">d’endettement</option> </select></span>, il accroît fortement son endettement public. La BCE est alors soumise à des contraintes dans le choix de sa politique monétaire.</p> 
     <p>En effet, si la BCE souhaite mener une politique monétaire restrictive, elle accroît le risque <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ph3t30jc">d’insolvabilité</option> <option data-select-code="xk2gopnz">de solvabilité</option> <option data-select-code="drwsrj6c">d’emprunt</option> </select></span> de cet État, car la hausse des taux d’intérêt directeurs alourdit la charge de la dette de ce pays et empêche alors la réduction de son déficit budgétaire, donc de son endettement. Cette augmentation du risque de défaut de remboursement de la dette de cet État aurait des conséquences en chaîne sur l’ensemble de la zone puisque les banques de la zone qui détiennent ces titres de dette seraient fortement <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="lrxoqx7t">créditrices</option> <option data-select-code="qccodlb3">favorisées</option> <option data-select-code="gshe76g3">fragilisées</option> </select></span>.</p> 
     <p>La BCE se trouve donc contrainte de mettre en œuvre une politique monétaire <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="egzj0mu4">expansive</option> <option data-select-code="w4cvny5b">de rigueur</option> <option data-select-code="ebadyxxf">d’endettement</option> </select></span> qui, en baissant les taux d’intérêt directeurs, faciliterait le financement de la dette et en accélérant <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="febt7pm3">l’inflation</option> <option data-select-code="wvb7zh74">la déflation</option> <option data-select-code="ufvudnts">la désinflation</option> </select></span> dans la zone permettrait une <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="9gji7z19">dévalorisation</option> <option data-select-code="bj99lf6g">dévaluation</option> <option data-select-code="6us8vb3q">valorisation</option> </select></span> de la dette de cet État. Or, dans la zone euro, la BCE a pour objectif premier la stabilité des prix, d’où des difficultés de coordination.</p> 
    </div>
   </div>
   <script src="https://cdn-static.pearltrees.com/econofides/js/bundle.js@date=pearltrees"></script>
   <style>
        .js-mcq .mcq-correct .mcq-feedback:before {
            content: "Correct !";
        }
        .js-mcq .mcq-incorrect .mcq-feedback:before {
            content: "Incorrect";
        }
        .js-mcq .mcq-partially-correct .mcq-feedback:before {
            content: "Vous n'avez pas sélectionné toutes les bonnes réponses.";
        }
    .medals{display:none;}
</style>
  </div>
 </body>
</html>