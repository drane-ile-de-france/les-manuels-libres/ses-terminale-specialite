<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
 <head>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="canonical" href="https://www.sciencespo.fr/department-economics/econofides"> 
  <meta property="og:title" content="Chapitre 5 Quelles politiques économiques dans le cadre européen&nbsp;?"> 
  <meta property="og:type" content="website"> 
  <meta property="og:url" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/text/05.html"> 
  <meta property="og:description" content="Quelles ont été les étapes de la construction du projet européen et comment son organisation d’aujourd’hui peut-t-il engendrer des difficultés quant à la coordination entre les Etats membres pour faire face à des chocs asymétriques&nbsp;?"> 
  <meta property="og:image" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/images/web/chapter-05-header.jpg"> 
  <link rel="stylesheet" type="text/css" media="all" href="https://cdn-static.pearltrees.com/econofides/terminale-ses/styles/web.css@date=20210916114607.css"> 
  <script>
        function delete_cookie(name) {
            document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
            console.log('Expired and removed MathJax menu cookie.');
        }
        delete_cookie('mjx.menu')
    </script> 
  <script id="MathJaxConfig" type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    "HTML-CSS": {
                        availableFonts: [], // force use of webFont
                        webFont: "Gyre-Pagella",
                        scale: 90,
                        styles: {
                            ".MathJax [style*=border-top-width]": {
                                "border-top-width": "0.5pt ! important"
                            }
                        }
                    }
                });
            </script> 
  <script id="MathJax" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML">
            </script>  
  <!-- End Matomo Code --> 
  <style> #MathJax_Message { display: none} .medals{display:none;}
</style>
  <title>Pearltrees :Question 5.8 Choisissez le terme exact pour compléter ce texte</title>
  <title></title>
 </head> 
 <body class="terminale-ses chapter" data-title="L’Économie pour Terminale SES" style="background-color:white;">
  <div id="wrapper">
   <div id="content">
    <div class="question" data-question="question-05-08">  
     <p>Si un pays membre de la zone euro subit un choc asymétrique <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="0o1d6zrj">négatif</option> <option data-select-code="uxzsk6gg">positif</option> <option data-select-code="97ufmhub">endogène</option> </select></span>, il doit mener une politique budgétaire expansionniste dans le but de relancer son activité ce qui favorisera également <em>in fine</em> la <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="g8hc4x55">croissance</option> <option data-select-code="htgcv91l">déflation</option> <option data-select-code="i8makqub">récession</option> </select></span> de la zone euro (du fait de l’interdépendance des économies). Mais les États membres ne sont pas nécessairement incités à mener une telle politique, car celle-ci va <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="ixuk7tv6">accroître</option> <option data-select-code="as3odk3g">réduire</option> <option data-select-code="sa85xuxd">stabiliser</option> </select></span> à court terme leur déficit public (par la hausse des dépenses publiques) et peut conduire à une dégradation de leur compétitivité <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="64c44pxh">prix</option> <option data-select-code="s8zswt0s">structurelle</option> <option data-select-code="4fyhegnm">hors prix</option> </select></span> via les tensions inflationnistes qu’elle génère.</p> 
     <p>Les États membres peuvent donc être incités à mener des politiques non coopératives qui auront un impact négatif sur l’activité économique des pays touchés par un choc asymétrique et <em>in fine</em> sur la croissance de la zone euro&nbsp;: il y a donc un <span class="select-list-wrapper"> <select class="select-list"> <option> &nbsp; </option> <option data-select-code="x2xd4snd">défaut</option> <option data-select-code="e0vztgwt">excès</option> <option data-select-code="tgg7mgh2">statut quo</option> </select></span> de coordination.</p> 
    </div>
   </div>
   <script src="https://cdn-static.pearltrees.com/econofides/js/bundle.js@date=pearltrees"></script>
   <style>
        .js-mcq .mcq-correct .mcq-feedback:before {
            content: "Correct !";
        }
        .js-mcq .mcq-incorrect .mcq-feedback:before {
            content: "Incorrect";
        }
        .js-mcq .mcq-partially-correct .mcq-feedback:before {
            content: "Vous n'avez pas sélectionné toutes les bonnes réponses.";
        }
    .medals{display:none;}
</style>
  </div>
 </body>
</html>