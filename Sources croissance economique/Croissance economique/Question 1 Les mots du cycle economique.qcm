<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
 <head>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="canonical" href="https://www.sciencespo.fr/department-economics/econofides"> 
  <meta property="og:title" content="Chapitre 1 Quels sont les sources et les défis de la croissance économique&nbsp;?"> 
  <meta property="og:type" content="website"> 
  <meta property="og:url" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/text/01.html"> 
  <meta property="og:description" content="Comment le capitalisme a révolutionné notre mode de vie et comment les sciences économiques nous permettent-t-elles de comprendre ce phénomène et les autres systèmes économiques&nbsp;?"> 
  <meta property="og:image" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/images/web/chapter-01-header.jpg"> 
  <link rel="stylesheet" type="text/css" media="all" href="https://cdn-static.pearltrees.com/econofides/terminale-ses/styles/web.css@date=20210916114607.css"> 
  <script>
        function delete_cookie(name) {
            document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
            console.log('Expired and removed MathJax menu cookie.');
        }
        delete_cookie('mjx.menu')
    </script> 
  <script id="MathJaxConfig" type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    "HTML-CSS": {
                        availableFonts: [], // force use of webFont
                        webFont: "Gyre-Pagella",
                        scale: 90,
                        styles: {
                            ".MathJax [style*=border-top-width]": {
                                "border-top-width": "0.5pt ! important"
                            }
                        }
                    }
                });
            </script> 
  <script id="MathJax" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML">
            </script>  
  <!-- End Matomo Code --> 
  <style> #MathJax_Message { display: none} .medals{display:none;}
</style>
  <title>Pearltrees :Question 1.1 Choisissez les bonnes réponses</title>
  <title></title>
 </head> 
 <body class="terminale-ses chapter" data-title="L’Économie pour Terminale SES" style="background-color:white;">
  <div id="wrapper">
   <div id="content">
    <div class="question" data-question="question-01-01" data-question-code="77exaeoamhgt98fahbojgvs6lqye">  
     <p>Regardez de nouveau le <a href="01.html#graphique-1-3">Graphique 1.3</a>. En vous appuyant sur ces informations, lesquelles des affirmations suivantes sont correctes ?</p> 
     <ul class="mcq-options"> 
      <li>La phase d’expansion désigne le fait que le PIB réel augmente de plus en plus vite.</li> 
      <li>La crise économique désigne la chute brutale du PIB réel.</li> 
      <li>La phase de ralentissement désigne le fait que le PIB réel baisse.</li> 
      <li>La dépression désigne une baisse durable et marquée du PIB réel contrairement à la récession qui est une baisse temporaire et souvent moins marquée du PIB réel.</li> 
     </ul> 
     <ul class="mcq-feedback"> 
      <li>En effet, la phase d’expansion désigne le fait qu’année après année le taux de croissance du PIB réel augmente. C’est donc un phénomène qui désigne l’accélération du rythme de la croissance au cours du temps sur une période plus ou moins longue.</li> 
      <li>La crise économique désigne au sens strict le point de retournement du cycle économique. Au sens courant, la crise désigne la période de stagnation durable du niveau de production ou de la baisse de celui-ci.</li> 
      <li>La phase de ralentissement désigne le fait que le PIB réel augmente, mais de moins en moins vite. En revanche, il ne diminue pas.</li> 
      <li>En effet, si la phase de récession se prolonge et s’amplifie, l’économie plonge en dépression.</li> 
     </ul> 
    </div>
   </div>
   <script src="https://cdn-static.pearltrees.com/econofides/js/bundle.js@date=pearltrees"></script>
   <style>
        .js-mcq .mcq-correct .mcq-feedback:before {
            content: "Correct !";
        }
        .js-mcq .mcq-incorrect .mcq-feedback:before {
            content: "Incorrect";
        }
        .js-mcq .mcq-partially-correct .mcq-feedback:before {
            content: "Vous n'avez pas sélectionné toutes les bonnes réponses.";
        }
    .medals{display:none;}
</style>
  </div>
 </body>
</html>