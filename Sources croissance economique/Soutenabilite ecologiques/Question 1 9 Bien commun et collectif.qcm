<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
 <head>  
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="canonical" href="https://www.sciencespo.fr/department-economics/econofides"> 
  <meta property="og:title" content="Chapitre 1 Quels sont les sources et les défis de la croissance économique&nbsp;?"> 
  <meta property="og:type" content="website"> 
  <meta property="og:url" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/text/01.html"> 
  <meta property="og:description" content="Comment le capitalisme a révolutionné notre mode de vie et comment les sciences économiques nous permettent-t-elles de comprendre ce phénomène et les autres systèmes économiques&nbsp;?"> 
  <meta property="og:image" content="https://www.sciencespo.fr/department-economics/econofides/terminale-ses/images/web/chapter-01-header.jpg"> 
  <link rel="stylesheet" type="text/css" media="all" href="https://cdn-static.pearltrees.com/econofides/terminale-ses/styles/web.css@date=20210916114607.css"> 
  <script>
        function delete_cookie(name) {
            document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT;';
            console.log('Expired and removed MathJax menu cookie.');
        }
        delete_cookie('mjx.menu')
    </script> 
  <script id="MathJaxConfig" type="text/x-mathjax-config">
                MathJax.Hub.Config({
                    "HTML-CSS": {
                        availableFonts: [], // force use of webFont
                        webFont: "Gyre-Pagella",
                        scale: 90,
                        styles: {
                            ".MathJax [style*=border-top-width]": {
                                "border-top-width": "0.5pt ! important"
                            }
                        }
                    }
                });
            </script> 
  <script id="MathJax" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML">
            </script>  
  <!-- End Matomo Code --> 
  <style> #MathJax_Message { display: none} .medals{display:none;}
</style>
  <title>Pearltrees :Question 1.9 Choisissez les bonnes réponses</title>
  <title></title>
 </head> 
 <body class="terminale-ses chapter" data-title="L’Économie pour Terminale SES" style="background-color:white;">
  <div id="wrapper">
   <div id="content">
    <div class="question" data-question="question-01-09" data-question-code="01idaiaysjq25z8x40sa9xe95r">  
     <p>Lesquelles des affirmations suivantes sont correctes ?</p> 
     <ul class="mcq-options"> 
      <li>Un bien est rival si la consommation de ce bien par un individu diminue la quantité disponible pour les autres consommateurs.</li> 
      <li>Un bien est excluable s’il n’est pas possible d’empêcher un individu de consommer le bien en question.</li> 
      <li>Un bien collectif est non excluable et non-rival alors qu’un bien commun est non excluable mais rival.</li> 
      <li>La défense nationale est un bien commun.</li> 
     </ul> 
     <ul class="mcq-feedback"> 
      <li>En effet, un bien est considéré comme rival, car toute quantité du bien consommé par un agent économique diminue la quantité disponible pour un autre agent économique.</li> 
      <li>Non, un bien est considéré comme excluable s’il est au contraire possible d’exclure de sa consommation un individu qui ne participe pas à son financement.</li> 
      <li>En effet, un bien collectif, comme la défense nationale ou l’éclairage public, est non excluable (gratuit) et non-rival (la consommation des uns n’empêche pas celles des autres) alors qu’un bien commun est toujours non excluable (sans financement de la part de l’agent économique qui le consomme), mais il est rival, car en situation d’épuisement.</li> 
      <li>La défense nationale n’est pas un bien commun, mais un bien public. Elle se caractérise par la propriété de non excluabilité. On ne peut exclure de ce service l’agent qui ne contribue pas à son financement. Mais ce service est également non rival. Les militaires protègent l’ensemble du territoire et le fait que la population augmente, par exemple, ne réduit pas le degré de défense du territoire.</li> 
     </ul> 
    </div>
   </div>
   <script src="https://cdn-static.pearltrees.com/econofides/js/bundle.js@date=pearltrees"></script>
   <style>
        .js-mcq .mcq-correct .mcq-feedback:before {
            content: "Correct !";
        }
        .js-mcq .mcq-incorrect .mcq-feedback:before {
            content: "Incorrect";
        }
        .js-mcq .mcq-partially-correct .mcq-feedback:before {
            content: "Vous n'avez pas sélectionné toutes les bonnes réponses.";
        }
    .medals{display:none;}
</style>
  </div>
 </body>
</html>